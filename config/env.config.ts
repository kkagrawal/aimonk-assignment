import gitlabConfig from './gitlab.config'

export default {
  appName: process.env.APP_NAME || gitlabConfig.projectName || 'App',
  baseUrl: process.env.BASE_URL || gitlabConfig.pagesUrl || '/',
  apiUrl: process.env.API_URL || '/api',
}
