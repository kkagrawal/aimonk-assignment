export default {
  projectName: process.env.CI_PROJECT_TITLE,
  projectBase: process.env.CI_PROJECT_PATH_SLUG,
  pagesUrl: process.env.CI_PAGES_URL
    ? '/' + process.env.CI_PAGES_URL.split('/').slice(3).join('/')
    : null,
}
